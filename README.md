# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage 

### Environment variables  

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port to forward requests to (default: `9000`) 

show errors in shell script
`set -e`

docker need entrypoint 
tell nginx to run with daemon off to show logs
`nginx -g 'daemon off;'`

update template vars
`envsubst < etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf`


# linux 
`mkdir -p`
creates subdirectories if they don'r exist
/some/path

change ownership
`chown`
user and group
`nginx:nginx`

make file executable 
`chmod +x`

`sed 's/-release//'`
take off -release

https://chmod-calculator.com/

# Git Lab
`dind`
docker in docker service

nested command
`$(aws ecr get-login --no-include-email --region us-west-1)`

`before_script: []`
overrides before_script

`=~ //`
regex